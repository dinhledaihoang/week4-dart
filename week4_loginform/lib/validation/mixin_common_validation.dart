mixin CommonValidation {

  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return "Please input a valid email. Missing '@'.";
    }
    if (!value.contains('.')) {
      return "Please input a valid email. Missing '.'.";
    }
    return null;
  }

  String? validatePassword(String? value) {
    bool validateStructure(String value) {
      String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[!@#\$&*~]).{8,}$';
      RegExp regExp = new RegExp(pattern);
      return regExp.hasMatch(value);
    }
    if (value!.length < 8) {
      return "Password must be at least 8 characters long.";
    }

    if (!validateStructure(value)) {
      return "Password must contain at least 1 uppercase, 1 lowercase, 1 special character or symbol.";
    }

    return null;
  }
}